package at.hakwt.swp4.example;

public class Shape {

    private final Type type;

    public Shape(Type t) {
        this.type = t;
    }

    public Type getType() {
        return type;
    }
}
