package at.hakwt.swp4.example;

public class OpenClosedPrincipleMain {

    public static void main(String[] args) {
        DrawingService drawingService = new DrawingService();
	    Shape square = new Shape(Type.SQUARE);
        drawingService.draw(square);

        Shape circle = new Shape(Type.CIRCLE);
        drawingService.draw(circle);
    }
}
