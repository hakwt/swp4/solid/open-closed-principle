package at.hakwt.swp4.example;

public class DrawingService {

    public void draw(Shape s) {
        if ( s.getType() == Type.CIRCLE ) {
            drawCircle();
        } else if ( s.getType() == Type.SQUARE ) {
            drawSquare();
        }
    }

    private void drawSquare() {
        System.out.println("Drawing a square");
        System.out.println(" ---- ");
        System.out.println(" |  | ");
        System.out.println(" ---- ");
    }

    private void drawCircle() {
        System.out.println("Drawing a circle");
        System.out.println("  _   ");
        System.out.println(" / \\ ");
        System.out.println(" \\ / ");
        System.out.println("  -   ");
    }


}
