package at.hakwt.swp4.exercise;

public class Addition implements Operation {
    private final double left;
    private final double right;
    private double result = 0.0;

    public Addition(double left, double right) {
        this.left = left;
        this.right = right;
    }

    public void setResult(double result) {
        this.result = result;
    }

    public double getLeft() {
        return left;
    }

    public double getRight() {
        return right;
    }

    public double getResult() {
        return result;
    }
}